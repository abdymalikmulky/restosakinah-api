import React, {useState, useEffect} from 'react';
import {View} from 'react-native';
import {Text, Title, Button, Card} from 'react-native-paper';
import {Header} from '../../components';
import AsyncStorage from '@react-native-community/async-storage';
import {CommonActions} from '@react-navigation/native';

import styles from './style';

const Detail = ({navigation, theme}) => {
  const [user, setUser] = useState(null);

  useEffect(() => {
    async function fetchUser() {
      const userSession = await AsyncStorage.getItem('user');
      setUser(JSON.parse(userSession));
    }
    fetchUser();
  }, []);
  return (
    <View style={styles.container}>
      <Header
        isHome={false}
        navigation={navigation}
        title={'Berita'}
        subTitle={'Detail Berita'}
      />
      <View style={styles.content} />
    </View>
  );
};

export default Detail;
