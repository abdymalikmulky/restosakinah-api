    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo e(app_name()); ?></title>

    <!-- Custom fonts for this template-->
    
    <link href="<?php echo e(assets_url("")); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo e(assets_url("")); ?>vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(assets_url("css/bootstrap-select.css")); ?>">
    <link rel="stylesheet" href="<?php echo e(assets_url("css/bootstrap.css")); ?>">

    <!-- Custom styles for this template-->
    <link href="<?php echo e(assets_url("css/sb-admin-2.css")); ?>" rel="stylesheet">
    <link href="<?php echo e(assets_url("css/custom.css")); ?>" rel="stylesheet">
    <link href="<?php echo e(assets_url("vendor/datatables/dataTables.bootstrap4.min.css")); ?>" rel="stylesheet">
    <link href="<?php echo e(assets_url("css/alert/sweetalert.css")); ?>" rel="stylesheet">
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

<?php /**PATH /Applications/MAMP/htdocs/wellness-center/application/views/layouts/head.blade.php ENDPATH**/ ?>