

<?php $__env->startSection('content'); ?>
	<div class="container-fluid">
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800">Covid</h1>
			<a href="<?php echo e(base_url('covid/add')); ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Tambah Data Covid</i></a>
		</div>
    <?php if(isset($alertStatus)): ?>
    <div class="card mb-3 <?php if($alertStatus): ?> border-bottom-success <?php else: ?> border-bottom-danger <?php endif; ?> alert-box">
      <div class="card-body">
        <?php echo e($alertMessage); ?>

      </div>
    </div>
    <?php endif; ?>
		<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-list fa-sm"></i> Data Covid</h6>
		</div>
		<div class="card-body">
		<div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>#</th>
                <th>Data Covid</th>
                <th>Update Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $no=1;
              ?>
              <?php $__currentLoopData = $covids; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $covid): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                <tr>
                  <td><?php echo e($no); ?></td>
                  <td><?php echo e($covid->data_covid); ?></td>
                  <td><?php echo e($covid->update_date); ?></td>
                  <td>
                  <a href="<?php echo e(base_url("")); ?>covid/<?php echo e($covid->covid_id); ?>/edit"><button class="d-none d-sm-inline-block btn btn-sm btn-warning shadow-sm"><i class="fas fa-edit fa-sm text-white-50"></i> Edit Data</button></a>
                  <a href="<?php echo e(base_url('covid/'.$covid->covid_id.'/delete')); ?>"><button class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-edit fa-sm text-white-50"></i> Hapus Data</button></a>
                  </td>
                </tr>
                <?php
                  $no++;
                ?>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
		</div>
		</div>
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/wellness-center/application/views/covid/index.blade.php ENDPATH**/ ?>