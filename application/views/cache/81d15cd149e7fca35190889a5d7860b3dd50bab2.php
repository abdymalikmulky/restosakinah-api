

<?php $__env->startSection('content'); ?>
	<div class="container-fluid">
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800">Tambah News</h1>
		</div>
		<?php if(isset($alertStatus)): ?>
		<div class="card mb-3 border-bottom-danger alert-box">
	      <div class="card-body">
	        <?php echo e($alertMessage); ?>

	      </div>
	    </div>
	    <?php endif; ?>
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-list fa-sm"></i> Data Tambah News</h6>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<?php
					echo form_open('news', array('class' => 'form', 'id' => 'add'));
					?>
						<div class="row">
							<div class="col-md-4">
								<label>Judul Berita</label>
							</div>
							<div class="col-md-6">
								<input type="text" class="form-control" name="belum_sembuh" id="odp" placeholder="Judul Berita">
							</div>
						</div>
						
						<button class=" d-sm-inline-block btn btn-sm btn-block btn-success shadow-sm" style="margin:20px; float:right;"><i class="fas fa-save fa-sm text-white-50"></i> Simpan Data</i></button>
					<?php
					// echo form_submit('', 'Add');
					
					// echo form_input(array('type' => 'text', 'name' => 'username'));
					echo form_close();
					?>		
				</div>
			</div>
		</div>

	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/satgas-web/application/views/news/add.blade.php ENDPATH**/ ?>