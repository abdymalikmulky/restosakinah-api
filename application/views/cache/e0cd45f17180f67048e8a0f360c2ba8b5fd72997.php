

<?php $__env->startSection('content'); ?>
	<div class="container-fluid">
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800">News</h1>
			<a href="<?php echo e(base_url('news/add')); ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Tambah Data News</i></a>
		</div>
    <?php if(isset($alertStatus)): ?>
    <div class="card mb-3 <?php if($alertStatus): ?> border-bottom-success <?php else: ?> border-bottom-danger <?php endif; ?> alert-box">
      <div class="card-body">
        <?php echo e($alertMessage); ?>

      </div>
    </div>
    <?php endif; ?>
		<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-list fa-sm"></i> Data News</h6>
		</div>
		<div class="card-body">
		<div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>#</th>
                <th>Title</th>
                <th>Content</th>
                <th>Author</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $no=1;
              ?>
              <?php $__currentLoopData = $newss; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $news): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                <tr>
                  <td><?php echo e($no); ?></td>
                  <td><?php echo e($news->title); ?></td>
                  <td><?php echo e($news->content); ?></td>
                  <td><?php echo e($news->author); ?></td>
                  <td>
                  <a href="<?php echo e(base_url("")); ?>news/<?php echo e($news->news_id); ?>/edit"><button class="d-none d-sm-inline-block btn btn-sm btn-warning shadow-sm"><i class="fas fa-edit fa-sm text-white-50"></i> Edit Data</button></a>
                  <a href="<?php echo e(base_url('news/'.$news->news_id.'/delete')); ?>"><button class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-edit fa-sm text-white-50"></i> Hapus Data</button></a>
                  </td>
                </tr>
                <?php
                  $no++;
                ?>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
		</div>
		</div>
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/satgas-web/application/views/news/index.blade.php ENDPATH**/ ?>