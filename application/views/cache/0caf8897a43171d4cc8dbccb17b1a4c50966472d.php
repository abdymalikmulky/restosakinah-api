

<?php $__env->startSection('content'); ?>
	<div class="container-fluid">
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800 text-capitalize">Tambah <?php echo e($entity); ?></h1>
		</div>
		<?php if(isset($alertStatus)): ?>
		<div class="card mb-3 border-bottom-danger alert-box">
	      <div class="card-body">
	        <?php echo e($alertMessage); ?>

	      </div>
	    </div>
	    <?php endif; ?>
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-list fa-sm text-capitalize"></i> Data Tambah <?php echo e($entity); ?></h6>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<?php
					echo form_open_multipart($entity.'/'.$item->id.'/edit', array('class' => 'form', 'id' => 'add'));
					?>
						<div class="row form-group">
							<div class="col-md-2">
								<label>Judul</label>
							</div>
							<div class="col-md-10">
								<input type="text" class="form-control" name="news_title" placeholder="Masukkan judul berita" required value="<?php echo e($item->news_title); ?>">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-2">
								<label>Gambar</label>
							</div>
							<div class="col-md-10">
								<img src="<?php echo e(base_url('uploads/'.$item->news_image)); ?>" width="100px" />
								<input type="file" class="form-control" name="news_image"  placeholder="Masukkan nomor kontak">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-2">
								<label>Content</label>
							</div>
							<div class="col-md-10">
								<div id="editor">
									<?php echo $item->news_content ?>
								</div>
								<textarea  class="form-control" id="editor-textarea" name="news_content" placeholder="Masukkan content berita" required><?php echo $item->news_content ?></textarea>
							</div>
						</div>
						
						<button class=" d-sm-inline-block btn btn-sm btn-success shadow-sm" style="margin:20px; float:right;"><i class="fas fa-save fa-sm text-white-50"></i> Simpan</i></button>
					<?php
					// echo form_submit('', 'Add');
					
					// echo form_input(array('type' => 'text', 'name' => 'username'));
					echo form_close();
					?>		
				</div>
			</div>
		</div>

	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/wellness-center/application/views/news/edit.blade.php ENDPATH**/ ?>