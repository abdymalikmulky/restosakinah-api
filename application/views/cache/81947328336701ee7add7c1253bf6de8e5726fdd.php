

<?php $__env->startSection('content'); ?>
	<div class="container-fluid">
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800 text-capitalize">Edit <?php echo e($entity); ?></h1>
		</div>
		<?php if(isset($alertStatus)): ?>
		<div class="card mb-3 border-bottom-danger alert-box">
	      <div class="card-body">
	        <?php echo e($alertMessage); ?>

	      </div>
	    </div>
	    <?php endif; ?>
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-list fa-sm text-capitalize"></i> Data Edit <?php echo e($entity); ?></h6>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<?php
					echo form_open($entity.'/'.$item->id.'/edit', array('class' => 'form', 'id' => 'add'));
					?>
						<div class="row form-group">
							<div class="col-md-2">
								<label>Nama</label>
							</div>
							<div class="col-md-10">
								<input type="text" class="form-control" name="contact_label" id="contact_label" placeholder="Masukkan nama" required value="<?php echo e($item->contact_label); ?>">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-2">
								<label>Nomor</label>
							</div>
							<div class="col-md-10">
								<input type="text" class="form-control" name="contact_number" id="contact_number" placeholder="Masukkan nomor kontak" required value="<?php echo e($item->contact_number); ?>">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-2">
								<label>Deskripsi</label>
							</div>
							<div class="col-md-10">
								<textarea  class="form-control" name="contact_description" id="contact_description" placeholder="Masukkan deskripsi kontak" required><?php echo e($item->contact_description); ?></textarea>
							</div>
						</div>
						
						<button class=" d-sm-inline-block btn btn-sm btn-success shadow-sm" style="margin:20px; float:right;"><i class="fas fa-pen fa-sm text-white-50"></i> Edit</i></button>
					<?php
					// echo form_submit('', 'Add');
					
					// echo form_input(array('type' => 'text', 'name' => 'username'));
					echo form_close();
					?>		
				</div>
			</div>
		</div>

	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/wellness-center/application/views/contact/edit.blade.php ENDPATH**/ ?>