<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo e(base_url()); ?>">
    <div class="sidebar-brand-icon ">
        <i class="fas fa-desktop"></i>
    </div>
    <div class="sidebar-brand-text mx-3"><?php echo e(app_name()); ?></div>
    </a>
    

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
    MENU
    </div>

    <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo e(base_url('covid')); ?>" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-database"></i>
            <span>Data Covid</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo e(base_url('slide')); ?>" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-newspaper"></i>
            <span>Slides</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo e(base_url('news')); ?>" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-newspaper"></i>
            <span>News</span>
        </a>
    </li>
    
    <!-- Divider -->
    <hr class="sidebar-divider">

    
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul><?php /**PATH /Applications/MAMP/htdocs/satgas-web/application/views/layouts/side.blade.php ENDPATH**/ ?>