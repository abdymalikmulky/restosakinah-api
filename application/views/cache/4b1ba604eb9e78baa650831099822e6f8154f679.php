<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo e(app_name()); ?></title>
        <?php echo $__env->make('layouts.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </head>
    <body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <?php echo $__env->make('layouts.side', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content">
            <?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <!-- Begin Page Content -->
            <div class="container-fluid">
            <?php echo $__env->yieldContent('content'); ?>
            </div>
        </div>
        <!-- End of Main Content -->
        <footer class="sticky-footer bg-white">
        <?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </footer>
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <?php echo $__env->make('layouts.foot', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </body>
</html><?php /**PATH /Applications/MAMP/htdocs/wellness-center/application/views/layouts/master.blade.php ENDPATH**/ ?>