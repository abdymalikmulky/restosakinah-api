

<?php $__env->startSection('content'); ?>
	<div class="container-fluid">
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800 text-capitalize">Approval <?php echo e($entity); ?></h1>
		</div>
		<?php if(isset($alertStatus)): ?>
		<div class="card mb-3 border-bottom-danger alert-box">
	      <div class="card-body">
	        <?php echo e($alertMessage); ?>

	      </div>
	    </div>
	    <?php endif; ?>
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-list fa-sm text-capitalize"></i> Approval <?php echo e($entity); ?></h6>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<?php
					echo form_open_multipart($entity.'/'.$item->id.'/edit', array('class' => 'form', 'id' => 'add'));
					?>
						<div class="row form-group">
							<div class="col-md-2">
								<label>Approval Status</label>
							</div>
							<div class="col-md-10">
								<select name="aduan_status" class="form-control">
									<?php $__currentLoopData = STATUS_ADUAN; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($status); ?>" <?php if(strtolower($status)==strtolower($item->aduan_status)): ?> selected <?php endif; ?>><?php echo e($status); ?></option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
							</div>
						</div>
						<button class=" d-sm-inline-block btn btn-sm btn-success shadow-sm" style="margin:20px; float:right;"><i class="fas fa-save fa-sm text-white-50"></i> Simpan</i></button>
					<?php
					// echo form_submit('', 'Add');
					
					// echo form_input(array('type' => 'text', 'name' => 'username'));
					echo form_close();
					?>		
				</div>
			</div>
		</div>

		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-list fa-sm text-capitalize"></i> Detail <?php echo e($entity); ?></h6>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<div class="row form-group">
						<div class="col-md-2">
							<label>Pembuat</label>
						</div>
						<div class="col-md-10">
							<?php echo e($item->created_user); ?>

						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-2">
							<label>Kategori</label>
						</div>
						<div class="col-md-10">
							<?php echo e($item->kategori_label); ?>

						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-2">
							<label>Berita</label>
						</div>
						<div class="col-md-10">
							<?php echo e($item->aduan_berita); ?>

						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-2">
							<label>Lampiran</label>
						</div>
						<div class="col-md-10">
							<a href="<?php echo e($item->created_user); ?>">Lampiran</a>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-2">
							<label>Tanggal Pembuatan</label>
						</div>
						<div class="col-md-10">
							<?php echo e($item->created_at); ?>

						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/wellness-center/application/views/aduan/edit.blade.php ENDPATH**/ ?>