

<?php $__env->startSection('content'); ?>
	<div class="container-fluid">
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800">Covid Monitoring</h1>
		</div>
    <?php if(isset($alertStatus)): ?>
    <div class="card mb-3 <?php if($alertStatus): ?> border-bottom-success <?php else: ?> border-bottom-danger <?php endif; ?> alert-box">
      <div class="card-body">
        <?php echo e($alertMessage); ?>

      </div>
    </div>
    <?php endif; ?>
		<div class="card shadow mb-4">
  		<div class="card-header py-3">
  			<h6 class="m-0 font-weight-bold text-danger"><i class="fas fa-list fa-sm"></i> Import Data Paparan Covid</h6>
  		</div>
  		<div class="card-body">
    		<form action="<?php echo e(base_url('covid_monitoring/export')); ?>" method="POST" enctype="multipart/form-data" style="width: 500px; margin: 0 auto">
          <div class="" style="text-align: center;">
            <input type="file" name="file-terpapar" id="customFile" class="form-control">
          </div>
          <div style="text-align: center;">
            <input class="btn btn-danger" type="submit" name="submit" value="Import" style="margin-top: 16px;width: 200px;">
          </div>
        </form>
  		</div>
		</div>

    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-danger"><i class="fas fa-list fa-sm"></i> Log Data Paparan Covid</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Raw Data</th>
                    <th>Import Date</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $no=1;
                  ?>
                  <?php $__currentLoopData = $covid_monitorings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                    <tr>
                      <td><?php echo e($no); ?></td>
                      <td style="font-size: 4px"><?php echo e($item->data); ?></td>
                      <td><?php echo e($item->created_at); ?></td>
                    </tr>
                    <?php
                      $no++;
                    ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/pusicov-web/application/views/covid_monitoring/index.blade.php ENDPATH**/ ?>