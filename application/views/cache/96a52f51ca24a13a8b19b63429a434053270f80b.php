

<?php $__env->startSection('content'); ?>
	<div class="container-fluid">
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800 text-capitalize"><?php echo e($entity); ?></h1>
		</div>
    <?php if(isset($alertStatus)): ?>
    <div class="card mb-3 <?php if($alertStatus): ?> border-bottom-success <?php else: ?> border-bottom-danger <?php endif; ?> alert-box">
      <div class="card-body">
        <?php echo e($alertMessage); ?>

      </div>
    </div>
    <?php endif; ?>
		<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary text-capitalize"><i class="fas fa-list fa-sm"></i> Data <?php echo e($entity); ?></h6>
		</div>
		<div class="card-body">
		<div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>#</th>
                <th>Pelapor</th>
                <th>Unit</th>
                <th>Kategori</th>
                <th>Berita</th>
                <th>Lampiran</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $no=1;
              ?>
              <?php $__currentLoopData = $aduans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
              <?php 
              $dataUser =  json_decode($item->raw_user);
              ?>
                <tr>
                  <td><?php echo e($no); ?></td>
                  <td><?php echo e($item->created_user); ?><br>(<?php echo e($item->created_by); ?>)</td>
                  <?php if($dataUser->studyprogramname==""): ?> 
                  <td><?php echo e($dataUser->groupname); ?> - <?php echo e($dataUser->unitposition); ?></td>
                  <?php else: ?>
                  <td><?php echo e($dataUser->groupname); ?><br><?php echo e($dataUser->studyprogramname); ?> - <?php echo e($dataUser->facultyname); ?></td>
                  <?php endif; ?>
                  <td><?php echo e($item->kategori_label); ?></td>
                  <td>
                    <?php
                      if(strlen($item->aduan_berita) > 50) {
                        echo substr($item->aduan_berita, 0,50)."...<a href='".base_url($entity.'/'.$item->id)."/edit'>selengkapnya</a>";
                      } else {
                        echo $item->aduan_berita;
                      }
                    ?>
                  </td>
                  <td><a href="<?php echo e(base_url('uploads/'.$item->aduan_lampiran)); ?>">Lampiran</a></td>
                  <td><?php echo e($item->aduan_status); ?></td>
                  <td>
                  <a href="<?php echo e(base_url($entity.'/'.$item->id)); ?>/edit"><button class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"><i class="fas fa-check fa-sm text-white-50"></i> Approval</button></a>
                  <a onclick="confirmDelete('<?php echo e($entity); ?>', '<?php echo e($item->id); ?>')"><button class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-edit fa-sm text-white-50"></i> Hapus Data</button></a>
                  </td>
                </tr>
                <?php
                  $no++;
                ?>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
		</div>
		</div>
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/wellness-center/application/views/aduan/index.blade.php ENDPATH**/ ?>