<!DOCTYPE html>
<html lang="en">
<head>
  <title>{{app_name()}}</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
  <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
  <link href="{{ assets_url("login/vendor/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->
  <link href="{{ assets_url("login/fonts/font-awesome-4.7.0/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->
  <link href="{{ assets_url("login/fonts/iconic/css/material-design-iconic-font.min.css") }}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->
  <link href="{{ assets_url("login/vendor/animate/animate.css") }}" rel="stylesheet" type="text/css">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="{{ assets_url("login/vendor/css-hamburgers/hamburgers.min.css") }}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ assets_url("login/vendor/animsition/css/animsition.min.css") }}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ assets_url("login/vendor/select2/select2.min.css") }}">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="{{ assets_url("login/vendor/daterangepicker/daterangepicker.css") }}">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="{{ assets_url("login/css/util.css") }}">
  <link rel="stylesheet" type="text/css" href="{{ assets_url("login/css/main.css") }}">
<!--===============================================================================================-->
</head>
<body>
  
  <div class="limiter">
    <div class="container-login100" style="background-image: url('{{assets_url('login/images/bg-01.jpg')}}');">
      <div class="wrap-login100">
        @php
          $attributes = array('class' => 'login100-form validate-form', 'id' => 'login-form');
          echo form_open('login', $attributes);
        @endphp
          <span class="login100-form-logo">
            <img src="{{assets_url('login/images/logo-withtext.jpeg')}}" width="50px">
          </span>

          <span class="login100-form-title p-b-34 p-t-27">
            Log in
          </span>

          <div class="wrap-input100 validate-input" data-validate = "Enter username">
            <input class="input100" type="text" name="username" placeholder="Username">
            <span class="focus-input100" data-placeholder="&#xf207;"></span>
          </div>

          <div class="wrap-input100 validate-input" data-validate="Enter password">
            <input class="input100" type="password" name="password" placeholder="Password">
            <span class="focus-input100" data-placeholder="&#xf191;"></span>
          </div>

          <div class="contact100-form-checkbox">
            <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
          </div>

          <div class="container-login100-form-btn">
            <button class="login100-form-btn">
              Login
            </button>
          </div>

          @php 
          echo form_close(); 
          @endphp
      </div>
    </div>
  </div>
  

  <div id="dropDownSelect1"></div>
  
<!--===============================================================================================-->
  <script src="{{ assets_url("login/vendor/jquery/jquery-3.2.1.min.js") }}"></script>
<!--===============================================================================================-->
  <script src="{{ assets_url("login/vendor/animsition/js/animsition.min.js") }}"></script>
<!--===============================================================================================-->
  <script src="{{ assets_url("login/vendor/bootstrap/js/popper.js") }}"></script>
  <script src="{{ assets_url("login/vendor/bootstrap/js/bootstrap.min.js") }}"></script>
<!--===============================================================================================-->
  <script src="{{ assets_url("login/vendor/select2/select2.min.js") }}"></script>
<!--===============================================================================================-->
  <script src="{{ assets_url("login/vendor/daterangepicker/moment.min.js") }}"></script>
  <script src="{{ assets_url("login/vendor/daterangepicker/daterangepicker.js") }}"></script>
<!--===============================================================================================-->
  <script src="{{ assets_url("login/vendor/countdowntime/countdowntime.js") }}"></script>
<!--===============================================================================================-->
  <script src="{{ assets_url("login/js/main.js") }}"></script>

</body>
</html>