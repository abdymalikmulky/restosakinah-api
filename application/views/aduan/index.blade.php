@extends('layouts/master')

@section('content')
	<div class="container-fluid">
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800 text-capitalize">{{$entity}}</h1>
		</div>
    @if(isset($alertStatus))
    <div class="card mb-3 @if($alertStatus) border-bottom-success @else border-bottom-danger @endif alert-box">
      <div class="card-body">
        {{$alertMessage}}
      </div>
    </div>
    @endif
		<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary text-capitalize"><i class="fas fa-list fa-sm"></i> Data {{$entity}}</h6>
		</div>
		<div class="card-body">
		<div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>#</th>
                <th>Pelapor</th>
                <th>Unit</th>
                <th>Kategori</th>
                <th>Berita</th>
                <th>Lampiran</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @php
                $no=1;
              @endphp
              @foreach ($aduans as $item) 
              @php 
              $dataUser =  json_decode($item->raw_user);
              @endphp
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$item->created_user}}<br>({{$item->created_by}})</td>
                  @if($dataUser->studyprogramname=="") 
                  <td>{{$dataUser->groupname}} - {{$dataUser->unitposition}}</td>
                  @else
                  <td>{{$dataUser->groupname}}<br>{{$dataUser->studyprogramname}} - {{$dataUser->facultyname}}</td>
                  @endif
                  <td>{{$item->kategori_label}}</td>
                  <td>
                    @php
                      if(strlen($item->aduan_berita) > 50) {
                        echo substr($item->aduan_berita, 0,50)."...<a href='".base_url($entity.'/'.$item->id)."/edit'>selengkapnya</a>";
                      } else {
                        echo $item->aduan_berita;
                      }
                    @endphp
                  </td>
                  <td><a href="{{base_url('uploads/'.$item->aduan_lampiran)}}">Lampiran</a></td>
                  <td>{{$item->aduan_status}}</td>
                  <td>
                  <a href="{{ base_url($entity.'/'.$item->id) }}/edit"><button class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"><i class="fas fa-check fa-sm text-white-50"></i> Approval</button></a>
                  <a onclick="confirmDelete('{{$entity}}', '{{$item->id}}')"><button class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-edit fa-sm text-white-50"></i> Hapus Data</button></a>
                  </td>
                </tr>
                @php
                  $no++;
                @endphp
              @endforeach
            </tbody>
        </table>
    </div>
		</div>
		</div>
  </div>
@endsection