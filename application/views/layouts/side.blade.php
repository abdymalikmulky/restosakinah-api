<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{base_url()}}">
        <div class="sidebar-brand-icon ">
            <span class="login100-form-logo">
                <img src="{{assets_url('login/images/logo-withtext.jpeg')}}" width="50px">
            </span>
        </div>
    </a>
    

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
    GENERAL
    </div>

    <li class="nav-item">
        <a class="nav-link collapsed" href="{{base_url('contact')}}" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-address-book"></i>
            <span>Informasi Kontak</span>
        </a>
    </li>
     <li class="nav-item">
        <a class="nav-link collapsed" href="{{base_url('news')}}" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-newspaper"></i>
            <span>Berita</span>
        </a>
    </li>
    
    <!-- Divider -->
    <hr class="sidebar-divider">


    <div class="sidebar-heading">
    LAPORAN
    </div>
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{base_url('aduan_kategori')}}" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-list-alt"></i>
            <span>Kategori Aduan</span>
        </a>
    </li>
    
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{base_url('aduan')}}" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-list-alt"></i>
            <span>Laporan Aduan</span>
        </a>
    </li>
    
    
    <!-- Divider -->
    <hr class="sidebar-divider">

    
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>