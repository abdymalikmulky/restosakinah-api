  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Siap untuk keluar aplikasi?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Klik tombol "Logout" Jika Anda ingin keluar dari aplikasi.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="{{base_url('logout')}}">Logout</a>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Bootstrap core JavaScript-->
  <script src="{{ assets_url("") }}vendor/jquery/jquery.min.js"></script>
  <script src="{{ assets_url("") }}vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  
  <!-- Core plugin JavaScript-->
  <script src="{{ assets_url("") }}vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="{{ assets_url("") }}vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ assets_url("") }}js/sb-admin-2.min.js"></script>
  <script src="{{ assets_url("") }}js/app.js"></script>
  
  <!-- Custom scripts for all pages-->
  
  
  
  <!-- ADD REMOVE INPUT FIELD JQUERY -->
  {{-- <script src="{{ assets_url("") }}js/toast/jquery.min.js"></script> --}}

  
  <!-- Page level plugins -->
  <script src="{{ assets_url("") }}vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="{{ assets_url("") }}vendor/datatables/dataTables.bootstrap4.min.js"></script>
  
  <!-- Page level custom scripts -->
  <script src="{{ assets_url("") }}js/demo/datatables-demo.js"></script>
  
  <!-- custom sweetalert -->
  <script src="{{ assets_url("") }}js/alert/sweetalert-dev.js"></script>
  <script src="{{ assets_url("") }}js/alert/alert.js"></script>

  <script src="{{ assets_url("") }}js/selectsearch/bootstrap-select.js"></script>   
  <script src="{{ assets_url("") }}js/toast/bootstrap.min.js"></script>
    <!-- Page level plugins -->
  <script src="{{ assets_url("") }}vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="{{ assets_url("") }}js/demo/chart-area-demo.js"></script>
  <script src="{{ assets_url("") }}js/demo/chart-pie-demo.js"></script>
  
  <!-- Main Quill library -->
  <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>

  <script type="text/javascript">
   // Initialize QUill editor
    var quill = new Quill('#editor', {
      modules: {
        toolbar: [
          [{ header: [1, 2, 3, 4, 5, 6,  false] }],
          ['bold', 'italic', 'underline','strike'],
          ['image', 'code-block'],
          ['link'],
          [{ 'script': 'sub'}, { 'script': 'super' }],
          [{ 'list': 'ordered'}, { 'list': 'bullet' }],
          ['clean']
        ]
      },
      placeholder: 'Tuliskan isi berita...',
      theme: 'snow'  // or 'bubble'
    });

    quill.on('text-change', function(delta, source) {
      updateHtmlOutput()
    })

    // Return the HTML content of the editor
    function getQuillHtml() { return quill.root.innerHTML; }

    // Highlight code output


    function updateHtmlOutput()
    {
      let html = getQuillHtml();
        console.log ( html );
        $('#editor-textarea').html(html);
    }


    updateHtmlOutput()

  </script>