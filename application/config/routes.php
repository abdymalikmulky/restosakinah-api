<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'DashboardController';

/*LANDING PAGE*/


/*ADMIN PAGE*/
//Master Data//
$route = generateAdminRouteCRUD($route, "food", "FoodController");

//AUTH
$route['login']['GET'] = 'LoginController';
$route['login']['POST'] = 'LoginController/authenticate';
$route['api/login']['POST'] = 'LoginController/api_authenticate';
$route['logout'] = 'LoginController/logout';


function generateAdminRouteCRUD($route, $path, $controller) {	
	$route[$path]['GET'] = $controller;
	$route[$path.'/(:num)']['GET'] = $controller.'/show/$1';
	$route[$path.'/add']['GET'] = $controller.'/add_get';
	$route[$path]['POST'] = $controller.'/add_post';
	$route[$path.'/(:num)/edit']['GET'] = $controller.'/edit_get/$1';
	$route[$path.'/(:num)/edit']['POST'] = $controller.'/edit_post/$1';
	$route[$path.'/(:num)/delete']['GET'] = $controller.'/delete/$1';
	$route[$path.'/(:num)/toogleStatus/(:num)']['GET'] = $controller.'/toogleStatus/$1/$2';

	$apiPath = 'api/'.$path;
	$route[$apiPath]['GET'] = $controller.'/api_all';
	$route[$apiPath.'/(:num)']['GET'] = $controller.'/api_show/$1';
	$route[$apiPath.'/add']['GET'] = $controller.'/api_add_get';
	$route[$apiPath]['POST'] = $controller.'/api_add_post';
	$route[$apiPath.'/(:num)/edit']['POST'] = $controller.'/api_edit_post/$1';
	$route[$apiPath.'/(:num)/delete']['GET'] = $controller.'/api_delete/$1';
	
	return $route;
}

//OTHER
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;