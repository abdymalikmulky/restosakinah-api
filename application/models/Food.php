<?php
	require_once 'BaseModel.php';
	class Food extends BaseModel {

		public function __construct(){
			$this->table = parent::$tableFood;
			parent::__construct($this->table);
		}
		public function allByFilter($type) {
	   		$this->db->from($this->table);
	   		if($type !== '') {
		   		$this->db->where('food_type', $type);
	   		}
	   		$this->db->order_by('resto_foods_id', 'desc');
	   		if(isset($_GET['limit'])) {
	   			$this->db->limit($_GET['limit']);
	   		}
			$data = $this->db->get();
			return $data->result();
		}
	}

?>