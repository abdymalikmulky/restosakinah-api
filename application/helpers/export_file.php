<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
new PhpOffice\PhpSpreadsheet\Style\Font;
/**
 * Note
 * ============================================
 * Required Paremeter
 * -$fileHeader : the title describes the document
 * -$filename : export file name
 * -$datas : data to export
 * 
 * ============================================
 * Optional Parameter
 *  $additionalTableHeader = []
 * 
 *  $additionalParameter = [
 *          'additionalTableHeader' => $additionalTableHeader,    
 *          'border' => true,    
 *          'serialNumber' => true,    
 *      ];
 * remove additional parameter from array if didnt need
 * ============================================
 *  default border = true 
 *  default serialNumber = true 
 */
if (!function_exists('exportExcel'))
{
    function exportExcel($fileHeader,$filename,$datas,$additionalParameter=[])
	{

            $additionalTableHeader = isset($additionalParameter['additionalTableHeader']) && $additionalParameter['additionalTableHeader'] ? $additionalParameter['additionalTableHeader'] : "" ;
            $border = isset($additionalParameter['border']) ? $additionalParameter['border']: true;
            $serialNumber = isset($additionalParameter['serialNumber']) ? $additionalParameter['serialNumber'] : true;

            $intToAlphabet = array_flip(SPREADSHEET_ALPHABET);
            
            $style =[
                  'alignment' => [
                        'vertical' => Alignment::VERTICAL_CENTER,
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                    ]
                  ];

            //init
		$spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            //set file header
            if($fileHeader){
                  $sheet->setCellValue('A1', $fileHeader);
                  $sheet->getStyle('A1')->getFont()->setBold(true);
                  $titleStartPosition = 3;
                  $dataStartPosition = 4;
            }else{
                  $titleStartPosition = 1;
                  $dataStartPosition = 2;
            }

            $mergecell = [];
            $emptyValue = 0;

            //arrange additional table header into excel
            if($additionalTableHeader){
                  $titleStartPosition = $titleStartPosition + 1;
                  $dataStartPosition = $dataStartPosition + 1;

                  function checkDuplicate($countTableHeader){ 
                  // returns if the int > 1 
                  if($countTableHeader > 1) 
                        return TRUE; 
                  else 
                        return FALSE;  
                  } 

                  $countTableHeader = array_count_values($additionalTableHeader); 
                  $duplicateTableHeader = array_keys(array_filter($countTableHeader, "checkDuplicate"));
                  
                  //additional table header set
                  $columnStartAdditionalTableHeader= $serialNumber ? 2:1;
                  $tempStartMergeCell = "";
                  foreach ($additionalTableHeader as $key => $value) {
                        if(in_array($value,$duplicateTableHeader)){
                              $additionalTableHeaderMerge[$value]['end'] = $intToAlphabet[$columnStartAdditionalTableHeader].($titleStartPosition-1);
                              if($tempStartMergeCell != $value){
                                    $additionalTableHeaderMerge[$value]['end'] = $intToAlphabet[$columnStartAdditionalTableHeader].($titleStartPosition-1);
                                    $additionalTableHeaderMerge[$value]['start'] = $intToAlphabet[$columnStartAdditionalTableHeader].($titleStartPosition-1);
                                    $tempStartMergeCell = $value;
                                    
                              }
                        }
                        
                        if($border){
                              setBorder($sheet,($intToAlphabet[$columnStartAdditionalTableHeader].($titleStartPosition-1)),'all');
                        }
                        
                        if($serialNumber){
                              if($border){
                                    setBorder($sheet,($intToAlphabet[1].($titleStartPosition-1)),'all');
                              }
                              
                              $sheet->setCellValue($intToAlphabet[1].($titleStartPosition-1), '#');
                              $sheet->getStyle($intToAlphabet[1].($titleStartPosition-1))->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
                              
                              $cellStart =$intToAlphabet[1].($titleStartPosition-1);
                              $cellEnd = $intToAlphabet[1].$titleStartPosition;
                              $sheet->mergeCells($cellStart.':'.$cellEnd);
                        }
                        
                        $sheet->setCellValue($intToAlphabet[$columnStartAdditionalTableHeader].($titleStartPosition-1), $value);
                        $sheet->setCellValue($intToAlphabet[$columnStartAdditionalTableHeader].($titleStartPosition-1), $value);
                        $columnStartAdditionalTableHeader++;
                  }
                  
                  foreach ($additionalTableHeaderMerge as $key => $value) {
                        $cellStart = $value['start'];
                        $cellEnd = $value['end'];
                        $sheet->mergeCells($cellStart.':'.$cellEnd);
                        $sheet->getStyle($cellStart.':'.$cellEnd)->applyFromArray($style);
                  }
            }
            
            //arrange data into excel
            $headerCount = 0;
            foreach ($datas as $keys => $values) {
                 $columnStart= $serialNumber ? 2:1;
                 foreach ($values as $key => $value) {
                       $explode = explode('$',$key);
                       $keyHeader = $explode[0];
                       if($keys == 0){
                             //set border title & first data
                             if($border){
                                   //border title
                                   setBorder($sheet,($intToAlphabet[$columnStart].($keys+$titleStartPosition)),'all');
                                   
                                   //border first data
                                   setBorder($sheet,($intToAlphabet[$columnStart].($keys+$titleStartPosition+1)),'all');

                                    if($serialNumber){
                                          //border number column
                                          setBorder($sheet,($intToAlphabet[1].($keys+$titleStartPosition)),'all');
                                          
                                          //border first number
                                          setBorder($sheet,($intToAlphabet[1].($keys+$dataStartPosition)),'all');
                                    }
                              }

                              if($serialNumber){
                                    $sheet->setCellValue($intToAlphabet[1].($keys+$titleStartPosition), '#');
                                    $sheet->getStyle($intToAlphabet[1].($keys+$titleStartPosition))->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
                                    $sheet->setCellValue($intToAlphabet[1].($keys+$dataStartPosition), $keys+1);
                              }

                              
                              $sheet->setCellValue($intToAlphabet[$columnStart].($keys+$titleStartPosition), $keyHeader);
                              $sheet->setCellValue($intToAlphabet[$columnStart].($keys+$dataStartPosition), $value);
                              if($additionalTableHeader){
                                    if($additionalTableHeader[$headerCount] == $key){
                                          $cellStart = $intToAlphabet[$columnStart].(($keys+$titleStartPosition)-1);
                                          $cellEnd = $intToAlphabet[$columnStart].($keys+$titleStartPosition);
                                          $sheet->mergeCells($cellStart.':'.$cellEnd);
                                          $sheet->getStyle($cellStart.':'.$cellEnd)->applyFromArray($style);
                                    }
                              }

                              $headerCount++;
                        }else{
                              //set border second data ++
                              if($border){
                                    //border second data ++
                                    setBorder($sheet,($intToAlphabet[$columnStart].($keys+$titleStartPosition+1)),'all');
                                    
                                    if($serialNumber){
                                          //border second number ++
                                          setBorder($sheet,($intToAlphabet[1].($keys+$dataStartPosition)),'all');
                                    }
                              }

                              if($serialNumber){
                                    $sheet->setCellValue($intToAlphabet[1].($keys+$dataStartPosition), $keys+1);
                              }
                              
                              if($value === "merge"){
                                    $mergecell[$keys][$emptyValue]['column'] = $columnStart;
                                    $mergecell[$keys][$emptyValue]['row'] = $keys+$dataStartPosition;
                                    $emptyValue++;
                              }

                              $sheet->setCellValue($intToAlphabet[$columnStart].($keys+$dataStartPosition), $value);
                        }
                        $columnStart++;
                  }
            }
            
            foreach ($mergecell as $key => $value) {
                  $cellStart = $serialNumber ? 'B'.$value[0]['row'] : 'A'.$value[0]['row'];
                  $cellEnd = $intToAlphabet[$value[array_key_last($value)]['column']].$value[array_key_last($value)]['row'];
                  $sheet->mergeCells($cellStart.':'.$cellEnd);
                  $sheet->getStyle($cellStart.':'.$cellEnd)->applyFromArray($style);
            }

            $writer = new Xlsx($spreadsheet);
            
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');
            $writer->save('php://output');
            die();
      }
      
      function setBorder($sheet,$cell,$position){
            switch ($position) {
                  case 'top':
                        $sheet->getStyle($cell)->getBorders()->getTop()->setBorderStyle(Border::BORDER_THIN);
                        return $sheet;
                  break;
                  case 'bottom':
                      $sheet->getStyle($cell)->getBorders()->getTop()->setBorderStyle(Border::BORDER_THIN);
                      return $sheet;
                  break;
                  case 'left':
                        $sheet->getStyle($cell)->getBorders()->getTop()->setBorderStyle(Border::BORDER_THIN);
                        return $sheet;
                  break;
                  case 'right':
                        $sheet->getStyle($cell)->getBorders()->getTop()->setBorderStyle(Border::BORDER_THIN);
                        return $sheet;
                  break;
                  default:
                      $sheet->getStyle($cell)->getBorders()->getTop()->setBorderStyle(Border::BORDER_THIN);
                      $sheet->getStyle($cell)->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                      $sheet->getStyle($cell)->getBorders()->getLeft()->setBorderStyle(Border::BORDER_THIN);
                      $sheet->getStyle($cell)->getBorders()->getRight()->setBorderStyle(Border::BORDER_THIN);
                      return $sheet;
                  break;
              }
      }
}