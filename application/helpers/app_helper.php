<?php
if (!function_exists('app_name')) {
    function app_name() {
        return get_instance()->config->item('app_name');
    }
}
if (!function_exists('app_desc')) {
    function app_desc() {
        return get_instance()->config->item('app_desc');
    }
}

?>