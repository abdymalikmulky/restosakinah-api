<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include 'BaseController.php';

class FoodController extends BaseController {
	protected $success = true;
	protected $message = '';
	protected $entity  = 'food';
	protected $entityId  = 'food_id';


	protected function getModel() {
		return $this->food;
	}

	protected function getEntityId() {
		return $this->entityId;
	}

	public function index($type="html")
    {
    	$foodType = '';
        $authStatus = $this->session->flashdata(KEY_STATUS);
        $authMessage = $this->session->flashdata(KEY_MESSAGE);

        if(isset($_GET['type'])) {
        	$foodType = $_GET['type'];
        }
        $data = $this->getModel()->allByFilter($foodType);
        $response = [
            'alertStatus' => $authStatus,
            'alertMessage' => $authMessage,
            'entity' => $this->entity,
            $this->entity.'s' => $data
        ];
        
        if($type=="json") {
            $this->apiResponse(true, API_MESSAGE_SUCCCESS, $data);
            return true;
        } 
        return view ($this->entity.'.index',$response);
    }
}
?>
