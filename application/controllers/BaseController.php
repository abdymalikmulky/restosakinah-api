<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BaseController extends CI_Controller {
    private $headers = array();
	public function __construct() {
        parent::__construct();
        $route = $this->uri->segment(1);
        $this->authPageException($route);
        
        //check token in API
        if($route=="api") {
            $endpoint = $this->uri->segment(2);
            if($endpoint!=='login') {
                header('Access-Control-Allow-Origin: *');
                header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Userid,Token");
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

                $this->headers = getallheaders();
                // $userId = $this->headers['Userid'];
                $token = $this->headers['Token'];
                $isTokenValid = $this->user->checkToken(0, $token);
                if(!$isTokenValid) {
                    header("HTTP/1.1 401 Unauthorized");
                    $this->apiResponse(false, "UNAUTHORIZED", null);
                    die();
                }
            }
            
        }
    }

	private function authPageException($route) {
        
        $isUserLoggedIn = $this->session->has_userdata(KEY_USER);        
        
        if($isUserLoggedIn && $route=="login") {
            redirect('/');
        } else if(!$isUserLoggedIn 
            && $route != "login" 
            && $route != "authenticate"
            && $route != "api"){
            redirect('/login');
        }
    }
    
    public function createResponse($success,$message,$key,$data) {
		$response['success'] = $this->success;
        $response['message'] = $this->message;
        $this->session->set_flashdata(KEY_STATUS, $this->success);
        $this->session->set_flashdata(KEY_MESSAGE, $message);
        if($key){
            $response[$key]     =  $data;
        }

        return (object) $response;
	}

    public function index($type="html")
    {

        $authStatus = $this->session->flashdata(KEY_STATUS);
        $authMessage = $this->session->flashdata(KEY_MESSAGE);

        $data = $this->getModel()->all();
        $response = [
            'alertStatus' => $authStatus,
            'alertMessage' => $authMessage,
            'entity' => $this->entity,
            $this->entity.'s' => $data
        ];
        
        if($type=="json") {
            $this->apiResponse(true, API_MESSAGE_SUCCCESS, $data);
            return true;
        } 
        return view ($this->entity.'.index',$response);
    }
    public function api_all() {
        $this->index("json");
    }

    public function apiResponse($success, $message, $data){
        header("Content-type:application/json");
        $res = [
            'success' => $success,
            'message' => $message,
            'data' => $data
        ];
        echo json_encode($res);
    }
    
    public function add_get() {
        $authStatus = $this->session->flashdata(KEY_STATUS);
        $authMessage = $this->session->flashdata(KEY_MESSAGE);

        $data = [
            'alertStatus' => $authStatus,
            'alertMessage' => $authMessage,
            'entity' => $this->entity,
        ];

        return view ($this->entity.'.add', $data);  
    }

    public function add_post($type="html") {
        $this->message = "Data successfully entered.";
        $data = new stdClass();
        $entityId = $this->entityId;

        $dataPost = json_decode(file_get_contents("php://input"), true);
        
        if(isset($_FILES)) {
            $uploadFile = $this->do_upload($_FILES, '');
            if(!empty($uploadFile)) {
                $imageName = "";
                if(!$uploadFile['success']) {
                    $this->success = false;
                    $this->message = $uploadFile['message'];
                    $data = '';
                    $this->createResponse($this->success,$this->message,$this->entity,$data);
                    redirect($this->entity.'/add');
                }
                $imageName = str_replace(" ", "_", $uploadFile['photo']);
                $dataPost[$uploadFile['key']] = $imageName;
            }
        }
        
        $data->$entityId = $this->getModel()->insert($dataPost);
        
        if(!$data->$entityId){
            $this->success = false;
            $this->message = 'Data failed to be entered';
            $data = '';
        }
        $response = $this->createResponse($this->success,$this->message,$this->entity,$data);

        if($type=="json") {
            if($response->success) {
                $this->apiResponse(true, API_MESSAGE_SUCCCESS, $data);
                return true;
            } else {
                $this->apiResponse(false, API_MESSAGE_FAIL, []);
                return true;
            }
        }

        if($response->success){
            redirect($this->entity);
        }
        else{
            redirect($this->entity.'/add');
        }
    }
    public function api_add_post() {
        $this->add_post("json");
    }

    public function edit_get($id) {
        $authStatus = $this->session->flashdata(KEY_STATUS);
        $authMessage = $this->session->flashdata(KEY_MESSAGE);

        $data = $this->getModel()->find($id);

        $data = [
            'alertStatus' => $authStatus,
            'alertMessage' => $authMessage,
            'entity' => $this->entity,
            'item' => $data
        ];

        return view ($this->entity.'.edit', $data); 
    }
    
    public function edit_post($id, $type="html") {
        $this->message = "Data successfully updated.";
        $data = new stdClass();
        $entityId = $this->entityId;

        $dataPost = json_decode(file_get_contents("php://input"), true);

        if(isset($_FILES)) {
            $uploadFile = $this->do_upload($_FILES, '');
            if(!empty($uploadFile)) {
                $imageName = "";
                if(!$uploadFile['success']) {
                    $this->success = false;
                    $this->message = $uploadFile['message'];
                    $data = '';
                    $this->createResponse($this->success,$this->message,$this->entity,$data);
                    redirect($this->entity.'/'.$id.'/edit');
                }
                $imageName = str_replace(" ", "_", $uploadFile['photo']);
                $dataPost['news_image'] = $imageName;
            }
        }



        $data->$entityId = $this->getModel()->update($dataPost,$id);

        if($data->$entityId < 0){
            $this->success = false;
            $this->message = 'Data failed to be update';
            $data = '';
        }else{
            $data = (object) $dataPost;
        }

        $response = $this->createResponse($this->success,$this->message,$this->entity,$data);

         if($type=="json") {
            if($response->success) {
                $this->apiResponse(true, API_MESSAGE_SUCCCESS, $data);
                return true;
            } else {
                $this->apiResponse(false, API_MESSAGE_FAIL, []);
                return true;
            }
        }

        if($response->success){
            redirect($this->entity);
        }
        else{
            redirect($this->entity.'/'.$id.'/edit');
        }
    }
    public function api_edit_post($id) {
        $this->edit_post($id,"json");
    }
    
    public function show($id,$type="html"){
        $authStatus = $this->session->flashdata(KEY_STATUS);
        $authMessage = $this->session->flashdata(KEY_MESSAGE);

        $data = $this->getModel()->find($id);
        
        $res = [
            'alertStatus' => $authStatus,
            'alertMessage' => $authMessage,
            $this->entity => $data
        ];
        if($type=="json") {
            if($data) {
                $this->apiResponse(true, API_MESSAGE_SUCCCESS, $data);
            } else {
                $this->apiResponse(false, "NOT FOUND", $data);
            }
            return true;
        } 
        return view ($this->entity.'.show', $res); 
	}

    public function api_show($id) {
        $this->show($id, "json");
    }
    
    public function deactivate($id){
        $this->message = 'Data successfully deleted';
        $authStatus = $this->session->flashdata(KEY_STATUS);
        $authMessage = $this->session->flashdata(KEY_MESSAGE);

        $dataPost = ['status_aktif' => 0];
        $data = $this->getModel()->update($dataPost,$id);

        
        if($data < 0){
            $this->success = false;
            $this->message = 'Data failed to be delete';
            $data = '';
        }else{
            $data = (object) $dataPost;
        }

        $response = $this->createResponse($this->success,$this->message,$this->entity,$data);
        
        redirect($this->entity);
	}



    public function delete($id, $type="html") {
        $this->message = "Data successfully deleted.";
        $data = $this->getModel()->delete($id); 
        
        if(!$data){
            $this->success = false;
            $this->message = "Data failed to delete.";
        }
        
        $response = $this->createResponse($this->success,$this->message,$this->entity,$data);
        if($type=="json") {
            $this->apiResponse($response->success, $this->message, $data);
            return true;
        } 
        redirect($this->entity);
    }

    public function api_delete($id) {
        $this->delete($id,"json");
    }
  
    public function do_upload($files, $path)
    {
        $data = array();
        
        $now = date('dmYHis');
        $path = './uploads/'.$path;
        
        $config['upload_path']          = $path;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        // $config['max_size']             = 100;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        if(count($files) > 0){
            foreach ($files as $key => $value) {
                if($value['name']!="") {
                    $fileName = $value['name'];
                    $config['file_name'] = $fileName;
                    
                    $this->upload->initialize($config);
                    
                    if ( ! $this->upload->do_upload($key))
                    {   
                        $data['success'] = false;
                        $data['message'] = strip_tags($this->upload->display_errors());
                    }
                    else
                    {
                        $data['success'] = true;
                        $data['key'] = $key;
                        $data['photo'] = $fileName;
                    }
                }
            }
        }
        
        return $data;
    }

    public function isSuperadmin() {
    }

}
