// Comingsoon link
$('.coming-soon').click(function(){
	swal({
	  title: "Next Feature",
	  text: "Fitur Akan dikembangkan pada tahap selanjutnya.",
	  type: "warning"
	});
	return false;
});
$('.not-allowed').click(function(){
	swal({
	  title: "Tidak Diizinkan",
	  text: "Hubungi admin, anda tidak dapat menggunakan fitur ini.",
	  type: "warning"
	});
	return false;
});