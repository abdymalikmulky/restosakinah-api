function confirmDelete(entity,id){
       
	swal({
		title: "Are you sure?",
		text: "This data will be deleted permanentyly.",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: '#2E59D9',
		cancelButtonColor: '#d33',
		confirmButtonText: "Yes, delete it!",
		cancelButtonText: "Cancel",
		closeOnConfirm: false,
		closeOnCancel: false
		},
		function(isConfirm){
		if (isConfirm) {
			window.location.href = entity+"/"+id+"/delete";
		} else {
			swal("Cancelled", "Data tidak terhapus", "error");
		}
		});
}

$( document ).ready(function() {
	

});

$(".form_datetime").datetimepicker({
	format: "yyyy-mm-dd hh:ii",
	// linkField: "mirror_field",
	linkFormat: "yyyy-mm-dd hh:ii",
	showMeridian: true,
	autoclose: true,
	todayBtn: true
});
